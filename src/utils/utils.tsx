import Color from 'color';
import {
  TDataCategory,
  TDataWordpress,
  callGetBlogs,
  callGetBlogsByCategory,
  callGetCategories,
} from '@/services/news';
import { GetServerSidePropsContext } from 'next';
import { TDataNews } from '@/pages/news';

export const getServerSideProps = async (
  context: GetServerSidePropsContext,
) => {
  console.log('context', context);
  //Call API Blog Wordpress
  const resBlog = await callGetBlogs(1, 100);
  const newBlogList = resBlog
    ? resBlog.data.map((item: TDataWordpress) => ({
        id: item.id,
        date: item.date,
        modified: item.modified,
        title: item.title.rendered,
        excerpt: item.excerpt.rendered,
        content: item.content.rendered,
        categoryName: item._embedded['wp:term'][0][0].name,
        categoryId: item._embedded['wp:term'][0][0].id,
        image:
          item._embedded && item._embedded['wp:featuredmedia']
            ? item._embedded['wp:featuredmedia'][0].source_url
            : '',
      }))
    : [];
  //Call API Blog Category Wordpress
  const resCategory = await callGetCategories();
  const newCategoryList = resCategory
    ? resCategory.data
        .filter((item: TDataCategory) => item.id !== 1)
        .map((item: TDataCategory) => ({
          id: item.id,
          name: item.name,
        }))
    : [];

  //Call API Blog By Category Wordpress
  const resBlogsByCategory = await callGetBlogsByCategory(Number(4), 1, 6);
  const newBlogsByCategory = resBlogsByCategory
    ? resBlogsByCategory.data
        .filter((item: TDataWordpress) => item.id !== 1)
        .map((item: TDataWordpress) => ({
          id: item.id,
          date: item.date,
          modified: item.modified,
          title: item.title.rendered,
          excerpt: item.excerpt.rendered,
          content: item.content.rendered,
          categoryName: item._embedded['wp:term'][0][0].name,
          categoryId: item._embedded['wp:term'][0][0].id,
          image:
            item._embedded && item._embedded['wp:featuredmedia']
              ? item._embedded['wp:featuredmedia'][0].source_url
              : '',
        }))
    : [];

  const blogsByCategoryAll: { [key: string]: TDataNews[] } = {};

  // Loop through each category and call the API to get blogs
  for (const category of newCategoryList) {
    const resBlogsByCategoryAll = await callGetBlogsByCategory(
      category.id,
      1,
      6,
    );
    if (resBlogsByCategoryAll) {
      const blogs = resBlogsByCategoryAll.data
        .filter((item: TDataWordpress) => item.id !== 1)
        .map((item: TDataWordpress) => ({
          id: item.id,
          date: item.date,
          modified: item.modified,
          title: item.title.rendered,
          excerpt: item.excerpt.rendered,
          content: item.content.rendered,
          categoryName: item._embedded['wp:term'][0][0].name,
          categoryId: item._embedded['wp:term'][0][0].id,
          image: item._embedded['wp:featuredmedia']
            ? item._embedded['wp:featuredmedia'][0].source_url
            : '',
        }));

      // Add the blogs to the object with the category name as the key
      blogsByCategoryAll[category.name] = blogs;
    }
  }

  return {
    props: {
      newBlogList,
      newCategoryList,
      newBlogsByCategory,
      blogsByCategoryAll,
    },
  };
};

export const formatDateUTC = (date: string) => {
  const specificTimeZone = 'Asia/Jakarta';
  const options = { timeZone: specificTimeZone };
  const formattedDate = new Date(date).toLocaleString('en-US', options);

  return formattedDate;
};

export const formatDateTime = (date: string) => {
  const dateTimeString = new Date(date);
  const day = dateTimeString.getDate();
  const month = dateTimeString.getMonth() + 1;
  const year = dateTimeString.getFullYear();
  const formattedDay = day.toString().padStart(2, '0');
  const formattedMonth = month.toString().padStart(2, '0');

  return `${formattedDay}/${formattedMonth}/${year}`;
};

export const truncateString = (str: string, maxLength: number) => {
  if (str.length > maxLength) {
    return str.substring(0, maxLength - 3) + '...';
  }
  return str;
};

export const generateColor = (centerColor: string) => {
  const color = centerColor;

  const scale = {
    100: Color(color).lighten(0.6).hex(),
    200: Color(color).lighten(0.45).hex(),
    300: Color(color).lighten(0.3).hex(),
    400: Color(color).lighten(0.15).hex(),
    500: Color(color).hex(),
    600: Color(color).darken(0.15).hex(),
    700: Color(color).darken(0.3).hex(),
    800: Color(color).darken(0.45).hex(),
    900: Color(color).darken(0.6).hex(),
    950: Color(color).darken(0.7).hex(),
  };

  return scale;
};
