import { As } from '@chakra-ui/react';

export type TSubProfileMenu = {
  text: string;
  href: string;
};

export type TProfileMenu = {
  icon: As | undefined;
  text: string;
  href: string;
  menu?: { isOpen: boolean; data: TSubProfileMenu[] };
};
