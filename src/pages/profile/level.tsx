import DefaultLayout from '@/components/layouts/defaulLayout';
import { Flex } from '@chakra-ui/react';
import ProfileMenu from './menu';

const Level = () => {
  return (
    <DefaultLayout>
      <Flex gap={4} p={10} mx="auto" w="full" maxW={1328} bg="#16191f">
        <ProfileMenu />
        <Flex gap={4} direction="column" flex={1}>
          <Flex h="100px" bg="green.500" />
          <Flex gap={4} w="full">
            <Flex flex={1} h="200px" bg="yellow.500"></Flex>
            <Flex flex={1} h="200px" bg="blue.500"></Flex>
          </Flex>
          <Flex gap={4} w="full">
            <Flex flex={1} h="150px" bg="yellow.500"></Flex>
            <Flex flex={1} h="150px" bg="blue.500"></Flex>
          </Flex>
          <Flex h="100px" bg="gray.500" />
        </Flex>
      </Flex>
    </DefaultLayout>
  );
};

export default Level;
