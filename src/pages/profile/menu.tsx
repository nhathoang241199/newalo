import { Flex, Icon, Link, Text } from '@chakra-ui/react';
import { menuData } from '@/utils/constant';
import { TProfileMenu, TSubProfileMenu } from '@/utils/type';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import { BsChevronDown } from 'react-icons/bs';
import { useState } from 'react';

const ProfileMenu = () => {
  const [data, setData] = useState<TProfileMenu[]>(menuData);
  const router = useRouter();

  const handleOpenSubMenu = (name: string) => {
    const newData = data.map((item: TProfileMenu) => {
      if (item.text === name && item.menu) {
        item.menu.isOpen = !item.menu.isOpen;
      }
      return item;
    });
    setData(newData);
  };

  return (
    <Flex
      h="fit-content"
      direction="column"
      gap={2}
      p={4}
      rounded="xl"
      w={240}
      bg="#232c3d33"
    >
      {data.map((item: TProfileMenu) => (
        <Flex
          rounded="lg"
          bg={
            item.href == router.pathname ||
            item.menu?.data.some(
              (item: TSubProfileMenu) => item.href === router.pathname,
            )
              ? '#ffffff0a'
              : ''
          }
          key={item.text}
          direction="column"
        >
          <Link
            onClick={() => {
              if (item.href === '') {
                handleOpenSubMenu(item.text);
              }
            }}
            as={NextLink}
            href={item.href}
            _hover={{ textDecoration: 'none' }}
          >
            <Flex
              rounded="lg"
              cursor="pointer"
              _hover={{
                background:
                  'linear-gradient(90deg, rgba(255, 255, 255, 0.08) 1.89%, rgba(255, 255, 255, 0) 108.73%)',
              }}
              px={3}
              gap={2}
              h="50px"
              w="full"
              alignItems="center"
            >
              <Icon
                w={'24px'}
                h={'24px'}
                color={item.href == router.pathname ? 'white' : '#6c727e'}
                as={item.icon}
              />
              <Text
                color={item.href == router.pathname ? 'white' : '#6c727e'}
                fontWeight={600}
                fontSize={14}
              >
                {item.text}
              </Text>
              <Flex flex={1} />
              {item.menu && item.menu?.data?.length > 0 && (
                <Icon
                  transition="all 0.3s ease"
                  transform={item.menu?.isOpen ? 'rotate(-180deg)' : ''}
                  color={item.href == router.pathname ? 'white' : '#6c727e'}
                  as={BsChevronDown}
                />
              )}
            </Flex>
          </Link>

          {item.menu && item.menu?.data?.length > 0 && (
            <Flex
              transition="all 0.3s ease"
              h={item.menu?.isOpen ? 50 * item.menu?.data?.length : 0}
              direction="column"
            >
              {item.menu?.data?.map((subItem: TSubProfileMenu) => (
                <Link
                  overflow="hidden"
                  key={subItem.href}
                  as={NextLink}
                  href={subItem.href}
                  _hover={{ textDecoration: 'none' }}
                >
                  <Flex
                    rounded="lg"
                    cursor="pointer"
                    _hover={{
                      background:
                        'linear-gradient(90deg, rgba(255, 255, 255, 0.08) 1.89%, rgba(255, 255, 255, 0) 108.73%)',
                    }}
                    px={3}
                    gap={2}
                    h="50px"
                    w="full"
                    alignItems="center"
                  >
                    <Text
                      ml={8}
                      color={
                        subItem.href === router.pathname ? 'white' : '#6c727e'
                      }
                      fontWeight={600}
                      fontSize={14}
                    >
                      {subItem.text}
                    </Text>
                    <Flex flex={1} />
                  </Flex>
                </Link>
              ))}
            </Flex>
          )}
        </Flex>
      ))}
    </Flex>
  );
};

export default ProfileMenu;
