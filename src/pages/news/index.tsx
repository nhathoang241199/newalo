import DefaultLayout from '@/components/layouts/defaulLayout';
import {
  formatDateTime,
  getServerSideProps,
  truncateString,
} from '@/utils/utils'; // Sửa đường dẫn import
import { Flex, Grid, GridItem, Text } from '@chakra-ui/react';
import { InferGetStaticPropsType } from 'next';
import dynamic from 'next/dynamic';
import Image from 'next/image';
const Carousel = dynamic(() => import('@/components/carousel'), { ssr: false });

export type TDataNews = {
  id: number;
  date: string;
  modified: string;
  title: string;
  excerpt: string;
  content: string;
  categories: number[];
  image: string;
};

const News = ({
  newBlogList,
  newCategoryList,
  newBlogsByCategory,
  blogsByCategoryAll,
}: InferGetStaticPropsType<typeof getServerSideProps>) => {
  console.log(
    newBlogList,
    newCategoryList,
    newBlogsByCategory,
    blogsByCategoryAll,
  );
  return (
    <DefaultLayout>
      <Flex
        textColor="white"
        flexDirection="column"
        maxW={{ base: '100%', sm: '1140px' }}
        mx={{ base: '', lg: 'auto' }}
        marginBottom="50px"
        w="100%"
      >
        <Flex
          gap={4}
          w="100%"
          px={{ base: 5, lg: 2 }}
          bg="#16191f"
          direction={{ base: 'column', lg: 'row' }}
          marginTop={{ base: '20px', lg: '40px' }}
          overflowX="hidden"
        >
          <Carousel data={newBlogList} />

          <Flex flex={3} gap="7px" flexDirection="column">
            {newBlogList?.slice(0, 5).map((item: TDataNews, index: number) => (
              <Flex
                borderRadius="10px"
                h={{ base: '200px', lg: '90px' }}
                bg="rgba(255, 255, 255, 0.04)"
                key={index}
                gap="20px"
                overflow="hidden"
              >
                <Flex
                  minHeight={{ base: '187px', lg: '90px' }}
                  minWidth={{ base: '250px', lg: '120px' }}
                  borderRadius={'10px'}
                  overflow="hidden"
                >
                  <Flex
                    transition="transform 0.3s ease-in-out"
                    width={{ base: '250px', lg: '120px' }}
                    _hover={{
                      transform: 'scale(1.1)',
                    }}
                  >
                    <Image
                      alt={'banner'}
                      src={item.image}
                      width={120}
                      height={90}
                      style={{
                        width: 'auto',
                        height: 'auto',
                        borderRadius: '10px',
                        imageRendering: 'crisp-edges',
                      }}
                    />
                  </Flex>
                </Flex>

                <Flex flexDirection="column" justifyContent={'center'}>
                  <Text
                    fontSize="14px"
                    textTransform="capitalize"
                    textColor="white"
                    dangerouslySetInnerHTML={{
                      __html: truncateString(item?.title?.toLowerCase(), 50),
                    }}
                  />
                  <Text textColor="rgb(173, 173, 173);" fontSize="12px">
                    {item.date && formatDateTime(item.date)}
                  </Text>
                </Flex>
              </Flex>
            ))}
          </Flex>
        </Flex>
        <Flex marginTop={{ base: '48px', lg: '96px' }} flexDirection="column">
          <Text
            fontSize="28px"
            marginBottom="24px"
            textColor="white"
            fontWeight="700"
          >
            New Post
          </Text>
          <Grid
            px={{ base: 5, lg: 2 }}
            templateColumns={{
              base: 'repeat(1, 1fr)',
              md: 'repeat(2, 1fr)',
              lg: 'repeat(3, 1fr)',
            }}
            gap={6}
          >
            {newBlogList?.slice(0, 6).map((item: TDataNews, index: number) => {
              return (
                <GridItem
                  rounded="10px"
                  key={index}
                  w="100%"
                  h="100%"
                  maxH="382px"
                  bg="rgb(43, 47, 54)"
                >
                  <Flex
                    minHeight={{ base: '100px', lg: '100px' }}
                    minWidth={{ base: 'full', lg: 'full' }}
                    borderRadius={'10px'}
                    overflow="hidden"
                  >
                    <Flex
                      transition="transform 0.3s ease-in-out"
                      width={{ base: 'full', lg: 'full' }}
                      _hover={{
                        transform: 'scale(1.1)',
                      }}
                    >
                      <Image
                        src={item.image}
                        alt={`image-${item.title}`}
                        width={255}
                        height={100}
                        style={{
                          imageRendering: 'crisp-edges',
                          width: '100%',
                          height: '255px',
                        }}
                      />
                    </Flex>
                  </Flex>

                  <Flex padding="20px">
                    <Text
                      fontSize="16px"
                      textTransform="capitalize"
                      fontWeight="bold"
                      dangerouslySetInnerHTML={{
                        __html: truncateString(item?.title?.toLowerCase(), 60),
                      }}
                    />
                  </Flex>
                  <Flex justifyContent="space-between" mx="25px">
                    <Image
                      alt="logo-footer"
                      src="/images/Footer-Logo.svg"
                      width={200}
                      height={37}
                      style={{
                        imageRendering: 'crisp-edges',
                        width: '68px',
                        height: '24px',
                        marginLeft: '-10px',
                      }}
                    />
                    <Text textColor="rgb(173, 173, 173);" fontSize="12px">
                      {item.date && formatDateTime(item.date)}
                    </Text>
                  </Flex>
                </GridItem>
              );
            })}
          </Grid>
        </Flex>
        {Object.entries(blogsByCategoryAll).map(
          ([categoryId, blogs]: [string, TDataNews[]]) => (
            <Flex
              key={categoryId}
              marginTop={{ base: '48px', lg: '96px' }}
              flexDirection="column"
            >
              <Text
                fontSize="28px"
                marginBottom="24px"
                textColor="white"
                fontWeight="700"
              >
                {categoryId}
              </Text>
              <Grid
                px={{ base: 5, lg: 2 }}
                templateColumns={{
                  base: 'repeat(1, 1fr)',
                  md: 'repeat(2, 1fr)',
                  lg: 'repeat(3, 1fr)',
                }}
                gap={6}
              >
                {blogs?.map((item: TDataNews, index: number) => {
                  return (
                    <GridItem
                      rounded="10px"
                      key={index}
                      w="100%"
                      h="382px"
                      bg="rgb(43, 47, 54)"
                    >
                      <Flex
                        minHeight={{ base: '100px', lg: '100px' }}
                        minWidth={{ base: 'full', lg: 'full' }}
                        borderRadius={'10px'}
                        overflow="hidden"
                      >
                        <Flex
                          transition="transform 0.3s ease-in-out"
                          width={{ base: 'full', lg: 'full' }}
                          _hover={{
                            transform: 'scale(1.1)',
                          }}
                        >
                          <Image
                            src={item.image}
                            alt={`image-${item.title}`}
                            width={255}
                            height={100}
                            style={{
                              imageRendering: 'crisp-edges',
                              width: '100%',
                              height: '255px',
                            }}
                          />
                        </Flex>
                      </Flex>

                      <Flex padding="20px">
                        <Text
                          fontSize="16px"
                          textTransform="capitalize"
                          fontWeight="bold"
                          dangerouslySetInnerHTML={{
                            __html: truncateString(
                              item?.title?.toLowerCase(),
                              60,
                            ),
                          }}
                        />
                      </Flex>
                      <Flex justifyContent="space-between" mx="25px">
                        <Image
                          alt="logo-footer"
                          src="/images/Footer-Logo.svg"
                          width={200}
                          height={37}
                          style={{
                            width: '68px',
                            height: '24px',
                            marginLeft: '-10px',
                            imageRendering: 'crisp-edges',
                          }}
                        />
                        <Text textColor="rgb(173, 173, 173);" fontSize="12px">
                          {item.date && formatDateTime(item.date)}
                        </Text>
                      </Flex>
                    </GridItem>
                  );
                })}
              </Grid>
            </Flex>
          ),
        )}
      </Flex>
    </DefaultLayout>
  );
};

export default News;

export { getServerSideProps };
