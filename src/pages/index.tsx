import DefaultLayout from '@/components/layouts/defaulLayout';
import Link from 'next/link';

export default function Home() {
  return (
    <DefaultLayout>
      <Link href="/test" color="black">
        test
      </Link>
    </DefaultLayout>
  );
}
