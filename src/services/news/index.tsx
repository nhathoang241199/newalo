import axios from 'axios';

type Title = {
  rendered: string;
};

type Excerpt = {
  rendered: string;
  protected: boolean;
};

type Content = {
  rendered: string;
  protected: boolean;
};

type FeaturedMedia = {
  source_url: string;
};
type Term = {
  name: string;
  id: number;
};

export type TDataWordpress = {
  id: number;
  date: string;
  modified: string;
  title: Title;
  content: Content;
  excerpt: Excerpt;
  categories: number[];
  _embedded: {
    'wp:featuredmedia': FeaturedMedia[];
    'wp:term': Term[][];
  };
};
export type TDataCategory = {
  id: number;
  name: string;
};
// Parse your data

export const callGetBlogs = async (page: number, rowPerPage: number) => {
  try {
    const response = await axios.get(
      `${process.env.NEXT_PUBLIC_API_WORDPRESS_URL}/wp-json/wp/v2/posts?per_page=${rowPerPage}&page=${page}&_embed`,
    );

    return response;
  } catch (error) {
    return null;
  }
};

export const callGetCategories = async () => {
  try {
    const response = await axios.get(
      `${process.env.NEXT_PUBLIC_API_WORDPRESS_URL}/wp-json/wp/v2/Categories`,
    );

    return response;
  } catch (error) {
    return null;
  }
};

export const callGetBlogsByCategory = async (
  name: number,
  page: number,
  rowPerPage: number,
) => {
  try {
    const response = await axios.get(
      `${process.env.NEXT_PUBLIC_API_WORDPRESS_URL}/wp-json/wp/v2/posts?categories=${name}&per_page=${rowPerPage}&page=${page}&_embed`,
    );

    return response;
  } catch (error) {
    return null;
  }
};
