import React from 'react';
import Slider from 'react-slick';

import { Box, Flex, Text } from '@chakra-ui/react';
import { TDataNews } from '@/pages/news';
import { truncateString } from '@/utils/utils';

type TProps = {
  data: TDataNews[];
};
const settings = {
  dots: true,
  arrows: false,
  infinite: true,
  autoplay: true,
  speed: 3000,
  autoplaySpeed: 5000,
  slidesToShow: 1,
  slidesToScroll: 1,
  draggable: true,
  // appendDots: (dots: React.ReactNode) => <Box gap="0">{dots}</Box>,
  // customPaging: () => (
  //   <Flex
  //     marginTop="-30px"
  //     marginLeft="300px"
  //     bg="rgb(173, 173, 173)"
  //     rounded="100%"
  //     color="white"
  //     className="custom-dot"
  //     style={{}}
  //     w={2}
  //     h={2}
  //   ></Flex>
  // ),
  dotsClass: 'slick-dots',
  customDotClass: 'custom-dot',
};

const Carousel: React.FC<TProps> = ({ data }) => {
  return (
    <Box
      position={'relative'}
      height={'475px'}
      // width={{ base: '100%' }}
      flex={7}
      overflow={'hidden'}
      zIndex={2}
      transition="all 0.3s ease"
      borderRadius="10px"
    >
      <link
        rel="stylesheet"
        type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
      />
      <link
        rel="stylesheet"
        type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
      />
      <style>{`
        #responsive {
          display: flex;
          flex-wrap: wrap;
          gap: 4px; 
        }

        #responsive .slick-list {
          width: 100%; 
        }
        .custom-dot {
          gap: 0px !important;
        }
        .slick-dots li.slick-active .custom-dot {
          background-color: rgb(80, 80, 80) !important;
        }
        .slick-dots .custom-dot.slick-active {
          padding: 0 2px;
        }
        .slick-dots li {
          margin: 0 !important;
          padding: 0 !important;
        }
      `}</style>
      <Slider {...settings}>
        {data.map((item: TDataNews, index: number) => {
          return (
            <Flex
              transition="all 0.3s ease"
              key={index}
              h="475px"
              position="relative"
              backgroundPosition="center"
              backgroundRepeat="no-repeat"
              backgroundSize="cover"
              backgroundImage={`url(${item.image})`}
            >
              <Flex
                w="full"
                px="20px"
                bottom="0"
                position="absolute"
                flexDirection="column"
                _after={{
                  content: '""',
                  position: 'absolute',
                  bottom: 0,
                  left: 0,
                  right: 0,
                  height: '100%',
                  zIndex: 0,
                  background:
                    'linear-gradient(to top, rgba(0, 0, 0, 1) 0%, rgba(0, 0, 0, 0.9) 50%, rgba(0, 0, 0, 0) 100%)',
                }}
              >
                <Text
                  zIndex={2}
                  textColor="white"
                  fontSize="28px"
                  fontWeight="bold"
                  dangerouslySetInnerHTML={{
                    __html: item?.title,
                  }}
                />
                <Text
                  zIndex={2}
                  textColor="rgb(173, 173, 173)"
                  fontSize="14px"
                  marginY="12px"
                  id="excerpt-content"
                  dangerouslySetInnerHTML={{
                    __html: truncateString(item?.excerpt, 240).replace(
                      '[&hellip;]',
                      '...',
                    ),
                  }}
                />
              </Flex>
            </Flex>
          );
        })}
        {/* Add more slides as needed */}
      </Slider>
    </Box>
  );
};

export default Carousel;
