import { Flex } from '@chakra-ui/react';
import { FC, useState } from 'react';
import Sidebar from './sidebar';
import Footer from './footer';
import Menu from './menu';
import Header from './header';

type TProps = {
  children: React.ReactNode;
};

const DefaultLayout: FC<TProps> = ({ children }) => {
  const [isOpenSidebar, setIsOpenSideBar] = useState(true);
  return (
    <Flex w="100vw" h="100vh" position="relative" bg="gray.300">
      <Sidebar
        isOpenSidebar={isOpenSidebar}
        setIsOpenSideBar={setIsOpenSideBar}
      />
      <Header isOpenSidebar={isOpenSidebar} />
      <Menu isOpenSidebar={isOpenSidebar} setIsOpenSideBar={setIsOpenSideBar} />
      <Flex
        pl={{ base: 0, md: '64px', xl: isOpenSidebar ? 246 : '64px' }}
        boxSizing="border-box"
        mt="64px"
        direction="column"
        w="full"
        position="relative"
        transition="all 0.3s ease"
      >
        <Flex bg="#16191f" direction="column" flex={1} overflowY="scroll">
          {children}
          <Footer />
        </Flex>

        <Menu
          isOpenSidebar={isOpenSidebar}
          setIsOpenSideBar={setIsOpenSideBar}
        />
      </Flex>
    </Flex>
  );
};

export default DefaultLayout;
