import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Flex,
  Grid,
  GridItem,
  Icon,
  Popover,
  PopoverContent,
  PopoverTrigger,
  Text,
  useDisclosure,
} from '@chakra-ui/react';
import { IoClose } from 'react-icons/io5';
import { GoChevronLeft } from 'react-icons/go';
import Image from 'next/image';
import Link from 'next/link';
import { FaChevronDown } from 'react-icons/fa';
import { itemFlag } from './header';

type TProps = {
  isOpenSidebar: boolean;
  // eslint-disable-next-line no-unused-vars
  setIsOpenSideBar: (value: boolean) => void;
};

const menuItemsTop = [
  {
    title: 'BONUS CENTER',
    icon: '/icons/bonus-icon.svg',
    alt: '',
    href: '',
  },
  {
    title: 'Daily Checkin',
    icon: '/icons/lucky-spin-icon.svg',
    alt: '',
    href: '',
  },
  {
    title: 'Lucky Spin',
    icon: '/icons/daily-checkin-icon.svg',
    alt: '',
    href: '',
  },
];

const menuItemsMidle = [
  {
    title: 'Games',
    icon: '/icons/game-icon.svg',
    alt: '',
    href: '',
  },
  {
    title: 'Promotions',
    icon: '/icons/promotions-icon.svg',
    alt: '',
    href: '',
    isPromotions: true,
  },
  {
    title: 'Staking',
    icon: '/icons/staking-icon.svg',
    alt: '',
    href: '',
  },
  {
    title: 'VIP Club',
    icon: '/icons/club-icon.svg',
    alt: '',
    href: '',
  },
  {
    title: 'Referral',
    icon: '/icons/referral-icon.svg',
    alt: '',
    href: '',
  },
  {
    title: 'Support',
    icon: '/icons/support-icon.svg',
    alt: '',
    href: '',
  },
  {
    title: 'News',
    icon: '/icons/news-icon.svg',
    alt: '',
    href: '',
  },
];

const Sidebar: React.FC<TProps> = ({ isOpenSidebar, setIsOpenSideBar }) => {
  const { isOpen, onToggle, onClose } = useDisclosure();
  return (
    <Flex
      position={'fixed'}
      minW={{
        base: isOpenSidebar ? '100vw' : '0',
        md: isOpenSidebar ? 246 : '64px',
      }}
      h={'100%'}
      bg="#16191F"
      transition="all 0.3s ease"
      zIndex={10}
    >
      <Icon
        display={{ base: 'flex', md: 'none' }}
        onClick={() => {
          onClose();
          setIsOpenSideBar(false);
        }}
        w={8}
        h={8}
        position="absolute"
        top={4}
        right={4}
        as={IoClose}
        textColor={'white'}
      />
      <Icon
        display={{ base: 'none', md: 'flex' }}
        onClick={() => setIsOpenSideBar(!isOpenSidebar)}
        w={8}
        h={8}
        position="absolute"
        top={4}
        right={isOpenSidebar ? -4 : -2}
        as={GoChevronLeft}
        zIndex={10}
        cursor="pointer"
        transform={isOpenSidebar ? 'rotate(0deg)' : 'rotate(180deg)'}
        transition="all 0.3s ease"
        textColor={'white'}
      />

      <Flex
        transition={isOpenSidebar ? 'all 0.3s ease' : ''}
        padding={isOpenSidebar ? '12px 22px 12px 12px' : 0}
        flexDirection="column"
        w={isOpenSidebar ? '100%' : '0'}
        textColor="white"
        overflow="hidden"
        whiteSpace="nowrap"
      >
        <Flex w="full" alignItems="center" justifyContent="center" h="64px">
          <Image
            alt="logo-sidebar"
            src="/images/Footer-Logo.svg"
            width={200}
            height={37}
            style={{
              width: '200px',
              height: '37px',
              margin: 'auto',
            }}
          />
        </Flex>
        <Flex
          borderRadius="4px"
          padding="4px"
          bg="#FFFFFF0A"
          flexDirection="column"
        >
          {menuItemsTop.map((item, index) => {
            return (
              <Flex
                marginY={item.title == 'Daily Checkin' ? '4px' : ''}
                w="100%"
                h="44px"
                key={index}
                placeItems="center"
                cursor="pointer"
                borderRadius="4px"
                textColor={item.title == 'BONUS CENTER' ? '#FFFFFF' : '#E0E0E0'}
                bg={
                  item.title === 'BONUS CENTER'
                    ? 'radial-gradient(101.24% 646.87% at 0.26% 100%, rgba(7, 12, 23, 0) 0%, #A60887 100%)'
                    : ''
                }
                _hover={{
                  background:
                    item.title === 'BONUS CENTER'
                      ? 'radial-gradient(101.24% 646.87% at 0.26% 100%, rgba(7, 12, 23, 0) 0%, #A60887 100%), linear-gradient(270deg, rgba(255, 255, 255, 0.4) -2.78%, rgba(255, 255, 255, 0) 87.04%)'
                      : 'linear-gradient(270deg, rgba(255, 255, 255, 0.4) -2.78%, rgba(255, 255, 255, 0) 87.04%)',
                }}
                paddingX="8px"
              >
                <Flex w="full" className="" alignItems="center">
                  <Image
                    alt={item.alt}
                    src={item.icon}
                    width={30}
                    height={26.25}
                    style={{
                      width: '30px',
                      height: '26.25px',
                    }}
                  />
                  <Text
                    fontSize="14px"
                    fontWeight={item.title === 'BONUS CENTER' ? 700 : 500}
                    marginLeft="4px"
                  >
                    {item.title}
                  </Text>
                </Flex>
              </Flex>
            );
          })}
        </Flex>
        <Flex
          h="calc(100% - 64px - 148px - 64px )"
          w="full"
          overflow="auto"
          overflowX="hidden"
        >
          <Flex w="full" marginTop="18px" flexDirection="column">
            {menuItemsMidle.map((item, index) => {
              if (item.isPromotions == true) {
                return (
                  <Accordion
                    marginBottom="2px"
                    key={index}
                    defaultIndex={[0]}
                    allowMultiple
                  >
                    <AccordionItem border="0">
                      <AccordionButton
                        paddingX="8px"
                        _hover={{
                          background:
                            'linear-gradient(90deg, rgba(255, 255, 255, 0.08) 1.89%, rgba(255, 255, 255, 0) 108.73%)',
                        }}
                      >
                        <Image
                          alt={item.alt}
                          src={item.icon}
                          width={30}
                          height={26.25}
                          style={{
                            width: '30px',
                            height: '26.25px',
                          }}
                        />
                        <Text
                          fontSize="14px"
                          fontWeight={500}
                          marginLeft="4px"
                          textColor="#E0E0E0"
                        >
                          {item.title}
                        </Text>
                        <AccordionIcon ml="auto" />
                      </AccordionButton>
                      <AccordionPanel
                        padding="8px"
                        bg="rgba(224, 224, 224, 0.04)"
                        display="flex"
                        flexDirection="column"
                        alignItems="center"
                      >
                        <Link href="">
                          <Flex
                            style={{
                              overflow: 'hidden',
                              borderRadius: '4px',
                            }}
                            marginBottom="8px"
                          >
                            <Flex
                              style={{
                                transition: 'transform 0.3s ease-in-out',
                              }}
                              _hover={{
                                transform: 'scale(1.1)',
                              }}
                            >
                              <Image
                                alt={'item-1'}
                                src={'/images/promotions-item-1.svg'}
                                width={200}
                                height={56}
                                style={{
                                  height: '100%',
                                  width: '100%',
                                }}
                              />
                            </Flex>
                          </Flex>
                        </Link>

                        <Link href="">
                          <Flex
                            style={{
                              overflow: 'hidden',
                              borderRadius: '4px',
                            }}
                            marginBottom="8px"
                          >
                            <Flex
                              style={{
                                transition: 'transform 0.3s ease-in-out',
                              }}
                              _hover={{
                                transform: 'scale(1.1)',
                              }}
                            >
                              <Image
                                alt={'item-1'}
                                src={'/images/promotions-item-2.svg'}
                                width={200}
                                height={56}
                                style={{
                                  height: '100%',
                                  width: '100%',
                                }}
                              />
                            </Flex>
                          </Flex>
                        </Link>

                        <Link href="">
                          <Flex justifyContent="center" gap="8px">
                            <Text textColor="#E0E0E0" fontSize="12px">
                              All Promotions
                            </Text>
                            <Image
                              src="/icons/greater-icon.svg"
                              alt="greater-icon"
                              height={20}
                              width={20}
                              style={{
                                height: '20px',
                                width: '20px',
                              }}
                            ></Image>
                          </Flex>
                        </Link>
                      </AccordionPanel>
                    </AccordionItem>
                  </Accordion>
                );
              } else {
                return (
                  <Flex
                    marginY={item.title == 'Daily Checkin' ? '4px' : ''}
                    w="100%"
                    minHeight="44px"
                    key={index}
                    placeItems="center"
                    cursor="pointer"
                    borderRadius="4px"
                    textColor={
                      item.title == 'BONUS CENTER' ? '#FFFFFF' : '#E0E0E0'
                    }
                    _hover={{
                      background:
                        'linear-gradient(90deg, rgba(255, 255, 255, 0.08) 1.89%, rgba(255, 255, 255, 0) 108.73%)',
                    }}
                    paddingX="8px"
                  >
                    <Flex w="full" className="" alignItems="center">
                      <Image
                        alt={item.alt}
                        src={item.icon}
                        width={30}
                        height={26.25}
                        style={{
                          width: '30px',
                          height: '26.25px',
                        }}
                      />
                      <Text
                        fontSize="14px"
                        fontWeight={item.title === 'BONUS CENTER' ? 700 : 500}
                        marginLeft="4px"
                      >
                        {item.title}
                      </Text>
                    </Flex>
                  </Flex>
                );
              }
            })}
          </Flex>
        </Flex>
      </Flex>

      <Flex
        transition="all 0.3s ease"
        flexDirection="column"
        w={isOpenSidebar ? '0' : 'auto'}
        textColor="white"
        borderRadius="5px"
        paddingY="12px"
        display={{ base: 'none', md: 'flex' }}
        hidden={isOpenSidebar}
      >
        <Flex h="65px" w="65px"></Flex>
        {menuItemsTop.concat(menuItemsMidle).map((item, index) => {
          return (
            <Flex
              _hover={{
                background:
                  'linear-gradient(90deg, rgba(255, 255, 255, 0.08) 1.89%, rgba(255, 255, 255, 0) 108.73%)',
              }}
              mx="auto"
              key={index}
              padding="12px 14px"
            >
              <Link href={item.href}>
                <Image
                  alt={item.alt}
                  src={item.icon}
                  width={30}
                  height={26.25}
                  style={{
                    width: '30px',
                    height: '26.25px',
                  }}
                ></Image>
              </Link>
            </Flex>
          );
        })}
      </Flex>

      <Popover
        isOpen={isOpen}
        onOpen={onToggle}
        onClose={onClose}
        closeOnBlur={false}
        placement="top-start"
      >
        <PopoverTrigger>
          <Flex
            padding="12px 22px 12px 22px"
            position="fixed"
            cursor="pointer"
            h="64px"
            w={isOpenSidebar ? '100%' : '0'}
            hidden={!isOpenSidebar}
            display={{ base: 'flex', md: 'none' }}
            alignItems="center"
            gap="6px"
            bottom={0}
            bg="#16191F"
          >
            <Image
              alt="Flat-America"
              src="/icons/flagAmerica.svg"
              width={24}
              height={24}
            />
            <FaChevronDown size={10} color="rgba(173, 173, 173, 1)" />
          </Flex>
        </PopoverTrigger>

        <PopoverContent
          bg="rgba(35, 44, 61, 0.4)"
          borderRadius="8px"
          backdropFilter="blur(15px)"
          padding="12px 6px"
          border="none"
          width="366px"
          display={{ base: 'grid', md: 'none' }}
        >
          <Grid templateColumns="repeat(2, 1fr)" gap="6px">
            {itemFlag.map((flag, index) => {
              return (
                <GridItem
                  key={index}
                  _hover={{ background: 'rgb(173, 173, 173)' }}
                  w="100%"
                  borderRadius="5px"
                  cursor="pointer"
                  display="flex"
                >
                  <Flex alignItems="center" p="4px" gap="10px">
                    <Image
                      alt={flag.alt}
                      src={flag.src}
                      width={24}
                      height={24}
                    />
                    <Text fontSize="14px">{flag.title}</Text>
                  </Flex>
                </GridItem>
              );
            })}
          </Grid>
        </PopoverContent>
      </Popover>
    </Flex>
  );
};

export default Sidebar;
