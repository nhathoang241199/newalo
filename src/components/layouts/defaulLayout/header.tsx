import {
  Button,
  Flex,
  Grid,
  GridItem,
  Input,
  InputGroup,
  InputLeftElement,
  Popover,
  PopoverContent,
  PopoverTrigger,
  Text,
  useDisclosure,
} from '@chakra-ui/react';
import Image from 'next/image';
import React from 'react';
import { IoSearch } from 'react-icons/io5';
import { FaChevronDown } from 'react-icons/fa';

type TProps = {
  isOpenSidebar: boolean;
  // eslint-disable-next-line no-unused-vars
};

export const itemFlag = [
  {
    title: 'English',
    alt: 'Flag-America',
    src: '/icons/flagAmerica.svg',
  },
  {
    title: 'English',
    alt: 'Flag-America',
    src: '/icons/flagAmerica.svg',
  },
];

const Header: React.FC<TProps> = ({ isOpenSidebar }) => {
  const { isOpen, onToggle, onClose } = useDisclosure();
  return (
    <Flex
      transition="all 0.3s ease"
      pl={{
        base: 0,
        md: isOpenSidebar ? '246px' : '64px',
      }}
      alignItems="center"
      position={'fixed'}
      h="64px"
      w="100%"
      bg="#16191F"
      zIndex={5}
    >
      <Flex>
        <Flex
          display={{ base: 'none', sm: 'flex', lg: 'none' }}
          marginLeft={'10px'}
        >
          <IoSearch size={24} color="rgba(224, 224, 224, 1)" />
        </Flex>
        <InputGroup
          display={{ base: 'none', lg: 'flex' }}
          bg="rgba(255, 255, 255, 0.04)"
          w="280px"
          marginLeft={{ base: '10px', lg: '40px' }}
        >
          <InputLeftElement pointerEvents="none">
            <IoSearch size={19} color="rgba(224, 224, 224, 1)" />
          </InputLeftElement>
          <Input
            focusBorderColor="transparent"
            type="text"
            fontSize="14px"
            placeholder="Search..."
            _placeholder={{ color: 'rgba(224, 224, 224, 1)' }}
            border="none"
            color="rgba(224, 224, 224, 1)"
          />
        </InputGroup>
      </Flex>

      <Flex
        ml={'auto'}
        color="white"
        gap={{ base: '10px', md: '20px' }}
        marginRight={{ base: '10px', sm: '20px', md: '40px' }}
      >
        <Flex alignItems="center">
          <Text
            cursor="pointer"
            fontSize="14px"
            fontWeight="700"
            marginRight="12px"
          >
            Log in
          </Text>
          <Button
            textColor="white"
            fontWeight="700"
            letterSpacing="0.5px"
            boxSizing="border-box"
            outline="none"
            background="rgb(255, 133, 203)"
            height="36px"
            padding="12px 16px"
            fontSize="14px"
            borderRadius="4px"
            transition="all 0.3s ease"
            _hover={{
              bg: 'rgb(255, 133, 203)',
              transform: 'scaleX(1.05)',
            }}
          >
            Sign up
          </Button>
        </Flex>
        <Popover
          isOpen={isOpen}
          onOpen={onToggle}
          onClose={onClose}
          closeOnBlur={false}
          placement="top-start"
        >
          <PopoverTrigger>
            <Flex
              display={{ base: 'none', md: 'flex' }}
              w="68px"
              height="36px"
              backdropFilter="blur(2px)"
              padding="10px 8px"
              borderRadius="4px"
              border="1px solid rgb(35, 44, 61)"
              cursor="pointer"
              alignItems="center"
              gap="6px"
            >
              <Image
                alt="Flat-America"
                src="/icons/flagAmerica.svg"
                width={24}
                height={24}
              />
              <FaChevronDown size={10} color="rgba(173, 173, 173, 1)" />
            </Flex>
          </PopoverTrigger>

          <PopoverContent
            bg="rgba(35, 44, 61, 0.4)"
            borderRadius="8px"
            backdropFilter="blur(15px)"
            padding="12px 6px"
            border="none"
            width="366px"
          >
            <Grid templateColumns="repeat(2, 1fr)" gap="6px">
              {itemFlag.map((flag, index) => {
                return (
                  <GridItem
                    key={index}
                    _hover={{ background: 'rgb(173, 173, 173)' }}
                    w="100%"
                    borderRadius="5px"
                    cursor="pointer"
                    display="flex"
                  >
                    <Flex alignItems="center" p="4px" gap="10px">
                      <Image
                        alt={flag.alt}
                        src={flag.src}
                        width={24}
                        height={24}
                      />
                      <Text fontSize="14px">{flag.title}</Text>
                    </Flex>
                  </GridItem>
                );
              })}
            </Grid>
          </PopoverContent>
        </Popover>
        <Flex alignItems="center" display={{ base: 'flex', sm: 'none' }}>
          <IoSearch size={24} color="rgba(224, 224, 224, 1)" />
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Header;
