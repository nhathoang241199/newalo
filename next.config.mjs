/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  output: 'standalone',
  images: {
    domains: ['103.231.191.58', 'example.com'],
  },
};

export default nextConfig;
